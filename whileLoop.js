let i = 1;

while(i <= 5){
	// Mengepel dari lantai 1 sampai 5
	console.log(i);
	i++;
}

console.log(`-------------------------------`);
// -------------------------------

const maxFloorLevel = 5;
let floorLevel = 1;

function getRandomInt(min, max) {
	// body...
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

while (floorLevel <= maxFloorLevel) {
	let isSpotless = false;
	
	console.log(`Ngepel lantai, ${floorLevel}`);

	let godsWill = getRandomInt(0,1);

	if(godsWill === 0) isSpotless = true;

	if (isSpotless) floorLevel++;
}
