function solution(n) {
	let sum = 0;

	while (n) {
    	sum += n % 10;
   		n = Math.floor(n / 10);
   	}
   	return sum;
}

console.log(solution(1001));

// Solusi kedua
function solution2(n) {
	// Convert int to string
	const str = n.toString();
	console.log(str);

	// Split string
	const splitSTR = str.split('');
	console.log(splitSTR);

	let total = 0;

	for (var i = 0; i < splitSTR.length; i++) {
		
		// console.log(splitSTR[i]);

		let strToInt = parseInt(splitSTR[i]);

		total += strToInt;

	}

	return total;
}

console.log(solution2(1234567890));