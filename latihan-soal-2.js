function solution(n) {
	if (n >= 91 && n <= 100) {
		return "A";
	}
	else if (n < 91 && n >= 81 ){
		return "B";
	}
	else if (n < 81 && n >= 71){
		return "C";
	}
	else if (n < 71 && n >= 61){
		return "D";
	}
	else if (n < 61) {
		return "E";
	}
	else {
		return "Tidak Terdeteksi";
	}
}

console.log(solution(101));
// console.log(solution(70));